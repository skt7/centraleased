# -*- coding: utf-8 -*-
"""
Created on Sat Feb 24 08:11:58 2018

@author: asus
"""
import cv2
from main import faceDetect
import base64
import imutils
import numpy as np
import os
import pandas as pd

def predict(img):
    
    labels = open('index.txt').read().split()
    #print(labels)
    
    recognizer = cv2.createLBPHFaceRecognizer()
    recognizer.load('model.yml')
    
    face = faceDetect(img)
    label, score = recognizer.predict(face)
    
    #print(label, score)
    
    return label, labels[label]

def decode(imgData, disp = False):
    #imgData = open('base64.txt', 'rb').read()
    img = base64.b64decode(imgData)
    #file = open('temp.jpg', 'wb')
    #file.write(img)
    #file.close()
    img = np.fromstring(img, dtype=np.uint8)
    img = cv2.imdecode(img, 1)
    img = imutils.resize(img, width=500)
    
    if disp == True:
        cv2.imshow('', img)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        
    return img

def makeDB():
    
    path = 'database/'   
    databases = os.listdir(path)
    
    dbNames = [database.split('.')[0] for database in databases] 
    labels = open('index.txt').read().split()
    
    df = pd.DataFrame(index = [i for i in range(len(labels))], columns = ['name'] + dbNames)
    df['name'] = labels
    
    print('------then-------')
    print(df)
    
    for i, database in enumerate(databases):
    
        dbName = dbNames[i]
        
        ref = pd.read_csv(path + database)
        
        print('------' + str(dbName) + '-------')
        
    
        for i, row in ref.iterrows():   
            img = row['image']
            img = decode(img)
            #cv2.imwrite('temp.jpg', img)
            label, lblname = predict(img)
            #print(row['name'])
            #print(lblname)
            #print(df[name][label])
            df[dbName][label] = int(row['id'])         
        
        print('------now-------')
        print(df)  
        
        df.to_csv('final.csv', index=True, header=True, sep=',', float_format = '%d') 
        
if __name__ == '__main__':
	makeDB()