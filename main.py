# -*- coding: utf-8 -*-
"""
Created on Fri Feb 23 10:21:20 2018

@author: asus
"""

import cv2
import imutils
from imgaug import augmenters as iaa
from imutils import face_utils
import dlib
import math

detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor('shape_predictor_68_face_landmarks.dat')

features = {'jaw' : (0, 17),
            'right_eyebrow' : (17, 22),
            'left_eyebrow' : (22, 27),
            'nose' : (27, 36),
            'right_eye' : (36, 42),
            'left_eye' : (42, 48),
            'mouth' : (48, 68)}

def rotate(image, angle):
    dimg = iaa.Sequential([
            iaa.Affine(rotate=(90-angle)) 
    ]).augment_images([image])
    
    return dimg[0]

def rotPoints(shape):
    
    global features
    
    #finding nasal length
    nose = features['nose']
    pts = shape[nose[0]:nose[1]]
    x1, y1 = pts[0, 0], pts[0, 1]
    jaw = features['jaw']
    pts = shape[jaw[0]:jaw[1]]
    x2, y2 = pts[8, 0], pts[8, 1]    

    return x1, y1, x2, y2

def faceDetect(image, disp = False, path = False):
    
    global detector, predictor
     
    if path == True:
        image = cv2.imread(image)   
    
    image = imutils.resize(image, width=500)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    rects = detector(gray, 1)
    rect = rects[0]
    
    shape = predictor(gray, rect)
    shape = face_utils.shape_to_np(shape)
    
    x1, y1, x2, y2 = rotPoints(shape)
        
    angle = math.atan2(y2-y1, x2-x1)
    angle = angle * 180 / math.pi
        
    rotImg = rotate(gray, angle)
    
    #cv2.imshow('', rotImg)
    #cv2.waitKey(0)
    
    rects = detector(rotImg, 1)
    rect = rects[0]
    
    shape = predictor(rotImg, rect)
    shape = face_utils.shape_to_np(shape)
    
    mouth = features['mouth']
    pts = shape[mouth[0]:mouth[1]]
    
    x1, y1, x2, y2 = pts[0, 0], pts[0, 1], pts[6, 0], pts[6, 1]
    
    face = rotImg[shape[19][1]: shape[8][1], shape[0][0]: shape[16][0]]

    if disp == True:
        cv2.imshow('', face)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    
    return face
    

        
#print(predict('images/modi.jpg'))
#f = open('base64.txt', 'rb')
#data = f.read()
#img = decode(data, disp = True)    
#print(predict(img))  
#f.close()
        