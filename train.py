# -*- coding: utf-8 -*-
"""
Created on Sat Feb 24 08:09:43 2018

@author: asus
"""
import os
from main import faceDetect
import imutils
import cv2
import numpy as np
import pandas as pd

def train(oneShot = False):
    imgData = []
    labels = []
    
    datasets = os.listdir('dataset')
    
    for label, dataset in enumerate(datasets):
        
        #print(dataset)
        
        path = 'dataset/' + dataset + '/train/'
        
        images = os.listdir(path)
        
        for i, image in enumerate(images):
            try:
                imgPath = path + image
                face = faceDetect(imgPath)              
                face = imutils.resize(face, width = 200)              
                
                #print(face.shape)
                
                imgData.append(face)
                labels.append(label)
            except:
                pass
            
    recognizer = cv2.createLBPHFaceRecognizer()
    recognizer.train(imgData, np.array(labels))
    recognizer.save('model.yml')
    
    np.savetxt('index.txt', datasets, delimiter=" ", fmt="%s")